package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"

	"github.com/julienschmidt/httprouter"
)

type SoundRequest struct {
	SoundID int `json:"SoundID"`
}

var appCtx context.Context
var router *httprouter.Router
var wg sync.WaitGroup

type Server struct {
	serv *http.Server
}

func (me *Server) Start(url string) *httprouter.Router {

	// define routes
	router := httprouter.New()
	router.POST("/sound/play", me.handlePlaySound)

	// init server
	me.serv = &http.Server{
		Addr:    url,
		Handler: router,
	}
	me.serv.SetKeepAlivesEnabled(false)
	go func() {
		me.serv.ListenAndServe()
	}()

	return router
}

func init() {
	appCtx = context.Background()
	wg = sync.WaitGroup{}
	wg.Add(1)
}

func main() {
	// PUERTO
	url := ":8087"
	// IP
	ipLocal := extractLocalIp()

	// Se crea el Servidor
	servidor := Server{}
	servidor.Start(url)

	fmt.Printf("Servidor iniciado en %s%v\n", ipLocal, url)

	wg.Done()

	// blocks till canceled
	for {
		select {
		case <-appCtx.Done():
			return
		}
	}
}

/**
*  Funcion que maneja una peticion al servidor, extrae desde el cuerpo el mensaje que tiene que ser de la siguiente forma:
*  { "soundID" : <numero> }
*  El número que se extraiga corresponderá con un archivo mp3, localizado en la carpeta sonidos/<numero>.mp3
*  Al extraerlo, se construye la ruta para pasarsela como parámetro a omxplayer para reproducirlo
**/
func (me *Server) handlePlaySound(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Se lee el cuerpo desde la peticion
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Errorf("No se reconoce la entrada")
	}

	mensaje := SoundRequest{}

	// Se serializa a SoundRequest
	err = json.Unmarshal(body, &mensaje)

	if err != nil {
		fmt.Errorf("No se ha podido deserializar la respuesta")
	}

	play_sound(mensaje)
	if err != nil {
		fmt.Fprintln(os.Stderr, "No se ha podido reproducir el sonido", err)
	}
}

func play_sound(mensaje SoundRequest) {
	fmt.Printf("Reproducir sonido %v\n", mensaje.SoundID)

	idSoundStr := strconv.Itoa(mensaje.SoundID)
	// Se construye el comando -> `omxplayer -o local sonidos/<numero>.mp3`

	command := "omxplayer -o local sonidos/" + idSoundStr + ".mp3"
	fmt.Println("Exec: " + command)

	// Se ejecuta
	exe_cmd(command)
}

/**
* Ejecuta un comando cualquiera en el terminal, usado para llamar a omxplayer para reproducir el sonido
**/
func exe_cmd(cmd string) {
	fmt.Println("command is ", cmd)
	// splitting head => g++ parts => rest of the command
	parts := strings.Fields(cmd)
	head := parts[0]
	parts = parts[1:len(parts)]

	out, err := exec.Command(head, parts...).Output()
	if err != nil {
		fmt.Printf("%s", err)
	}
	fmt.Printf("%s", out)
}

func GetOutboundIP() net.IP {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP
}

/**
 * Extrae IP local para mostrarla cuando el servidor empieza, el nombre tiene que contener wlan (linux) o en0 (mac)
 */
func extractLocalIp() string {
	return GetOutboundIP().String()
}
